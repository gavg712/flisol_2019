FLISOL Loja 2019

Ponencia: 27-Abr-2019

![](img/flisol_flyer.png)

La ponencia está orientada a estudiantes universitarios o personas que requieren de levantamiento de información de campo. Se trata sobre una estrategia simple de recolección de información en campo con ODK Collect en Android y sistematización con Tidyverse en R. Esta estrategia de recolección asume que la persona que requiere datos de campo, no tiene acceso a servidores (hardware o software) que le permitan usar plataformas disponibles actualmente en Internet

Licencia

<a rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>.